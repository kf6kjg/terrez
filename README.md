A full-resolution height-map to [LLRAW][] converter for [Second Life][]® or [compatible][] virtual worlds.

  [LLRAW]: http://wiki.secondlife.com/wiki/Tips_for_Creating_Heightfields_and_Details_on_Terrain_RAW_Files
  [Second Life]: http://secondlife.com/
  [compatible]: http://opensimulator.org/

Most of us couldn't care less what an [LLRAW][] is.  We simply want to get our awesome terrain files,
made by tools like [World Machine][], into our region or estate as easily as possible. Traditionally
this has been an annoying, if not downright complex, task involving [several very complex steps][steps].

  [World Machine]: http://www.world-machine.com/

But now this is no longer an issue! Now we can simply load the heightmap image straight from our
favorite terrain generator, slide a few dials if we want, and hit the export button!  Check out the
[Tutorial][tut] for more information!

Been using TerRez and want to contribute? Clone the source, get some pull requests going - be sure to
check the [Roadmap][roadmap]! I'm afraid I'm not currently set up for translation, but I'm open to a pull
request with a usable translation system!

  [steps]: #markdown-header-the-old-way-things-had-to-be-done
## The old way things had to be done
_**Warning!!!** Algebra ahead!_

1. Load an existing .raw terrain heightmap into either [GIMP][] (you were able to fix/use
   [the plugin][plugin], right?) or Adobe's Photoshop.  In PS, you need to make sure that the raw
   importer gets the correct information...
2. Replace the topmost layer, channel in PS's case, in the long list of layers/channels that largely
   have no purpose any more with your heightmap - you did resize it to 256x256, correct?
   In GIMP's case make sure to keep the layer's name correct...
3. Adjust the color of the multiplier layer/channel, the second one in the list,
   using the following formula:
   *m = E * 128 / 255*
   Where *E* is the expected maximum terrain height from the lowest allowable sea floor
   (corresponds to the **Maximum Elevation** value in World Machine),
   and *m* is the brightness value that the multiplier layer/channel must be set at.
4. Export the file to a .raw file for upload into your region server.

Oh, yeah, did you want to split the terrain into multiple maps for use across an entire estate? That
happens separately before step 1....

  [GIMP]: http://www.gimp.org/
  [plugin]: http://dominodesigns.info/project/gimpterrain

**Thankfully this headache is no longer needed!**

  [tut]: #markdown-header-the-terrez-tutorial
## The TerRez Tutorial
1. Load your terrain heightmap image into TerRez using the **Load Image** button.
2. Set your maximum terrain elevation by adjusting the **Maximum Terrain Height** slider to the value you want.  If you are a [World Machine][] user, simply use the same value as you had set for the Maximum Elevation.
3. Make sure everything, especially your coastline, looks good on the map, and hit the **Export Terrain** button.

Nice and simple.

  [roadmap]: #markdown-header-the-terrez-roadmap
## The TerRez Roadmap
### Release 1.0 (WIP)
1. [Images other than 256px square](https://bitbucket.org/kf6kjg/terrez/issue/10/how-about-images-other-than-256px-square)
2. [HDR import](https://bitbucket.org/kf6kjg/terrez/issue/3/hdr-heightmap-support)
3. [Estate export support](https://bitbucket.org/kf6kjg/terrez/issue/1/add-ability-to-split-terrain-into-a-grid)
4. [Settings file](https://bitbucket.org/kf6kjg/terrez/issue/5/retain-settings)

### Release 2.0
1. [Alternate views of terrain](https://bitbucket.org/kf6kjg/terrez/issue/6/add-other-ways-of-seeing-the-terrain)
2. [Non-square terrains](https://bitbucket.org/kf6kjg/terrez/issue/4/what-about-non-square-terrains)

### The "Future"
Please browse the [issue tracker](https://bitbucket.org/kf6kjg/terrez/issues?responsible=%21&status=open&status=new)!
