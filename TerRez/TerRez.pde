#!/Users/ricky/processing-exec.sh

import g4p_controls.*;

/* * * * * * * * * * * * * * * * * * * * * * * * * */
// Variable declarations
/* * * * * * * * * * * * * * * * * * * * * * * * * */

JSONArray terrain_conversions;

// TODO: turn this stuff into a class.
boolean map_is_ready = false; // HACK: not a terribly useful method to create a lock...
int[] map;
final int MAP_MAX_VALUE = 0xFFFF;
final int MAP_MSB_SHIFT = 8;
int map_width;
PImage pixel_buffer;
boolean pixel_buffer_is_outdated = true;

GLabel label1;
GLabel label2;
GLabel label3;
GLabel label4;
GLabel label5;
GSlider terrain_height;
GLabel  terrain_height_readout;
GSlider water_height;
GLabel  water_height_readout;
GTextField region_count_x;
GTextField region_count_y;
GButton load_image;
GButton export_terrain;
GWindow map_window;

boolean has_booted;

int WINDOW_WIDTH = 400;
int WINDOW_HEIGHT = 320;

void setup() {
	size(WINDOW_WIDTH, WINDOW_HEIGHT, JAVA2D);
	has_booted = false;

	/*
	TODO: 
	* Create a gui that has the following:
	**x Numeric: Maximum expected terrain height in meters
	** (later)Radio: Terrain height as distance from seafloor to mighest peak
	** (later)Radio: Terrain height as distance above water to highest peak
	**x Numeric: Number of regions to cover along the X axis
	**x Numeric: Number of regions to cover along the Y axis
	**x Numeric: Water height in meters
	**x Button: Load map image (loads into a 16bit greyscale)
	** Text: File path
	** Text: File prefix
	**x Button: Export region tiles
	**x Drawing Area: map view (512xY, where Y=RegionCountY*512/RegionCountX)
	** (later)Drawing area: elevation view (Facing North, XxY, where X=, and Y=)
	** (later)Drawing area: elevation view (Facing East, XxY where X=, and Y=)
	** (later)Numeric: Region size in meters (default: 256)
	** (later)Drawing Area: 3D navigable view
	** (later)Drawing Area: Selectable export regions
	** (later)

	* While drawing in the map view:
	** Scale the map view (and draw appropriately) to match current settings.
	** Make sure to draw values that are above the water level in greyscale.
	** Make sure to draw values that are at or below the water level in monochrome blue.
	** Draw green grid lines to match the region counts.

	* When Export button is pressed:
	** Ask for confirmation: continue if affirmative, cancel otherwise.
	** Scale the image to the correct X and Y values: X=256*RegionCountX, Y=256*RegionCountY
	** Compute the meter height for each pixel in the image, and use that to pull up the corresponding Red and Green channel values.
	** Set Blue channel to the correct value for the requested water height.
	** Save to disk with all other channels set to correct default values

	(later)
	* While drawing in the elevation view:
	** Each shows the height of the terrain as viewed facing that direction.
	** Display as a simple axis-conversion, with black background:
	*** Facing North: XYZ -> +X +Z +Y
	*** Facing East:  XYZ -> -Z +X +Y
	** All Y-axis values at and below the water level are in blue channel only.
	** All other Y-axis values are greyscale (all channels equal).


	*/

	// Load the conversion data
	terrain_conversions = loadJSONArray("Value Lookup.json");

	// Create a basic map.
	map_width = 256;
	map = new int[map_width * map_width]; // 256 pixels x 256 pixels
	int value; int x; int y;
	for (int pix_index = 0; pix_index < map.length; ++pix_index) {
		y = pix_index / map_width;
		x = pix_index - y * map_width;
		//value = MAP_MAX_VALUE * cos(PI * x / 256.0); // 256 is the range of one byte.
		value = MAP_MAX_VALUE * x / 256; // 256 is the range of one byte.
		
		map[pix_index] = value;
	}
	map_is_ready = true;

	// Prepare the pixel buffer.
	pixel_buffer = new PImage();

	// Set system look and feel for Swing widgets.
	PDialog.setSystemLookAndFeel(true);

	// Create the G4P GUI
	createGUI();
	
	if (map_window == null) {
		int width = 256 * int(region_count_x.getText());
		int height = 256 * int(region_count_y.getText());
		map_window = new GWindow(this, "Map View", displayWidth/2, (displayHeight - WINDOW_HEIGHT) / 2, width, height, false, JAVA2D);
		PImage blank = createImage(width, height, RGB);
		map_window.setBackground(blank);
		//map_window.setActionOnClose(GWindow.CLOSE_WINDOW);
		map_window.setAutoClear(false);
		map_window.setOnTop(false);
	}
}


void draw() {
	if (this != null) {
		background(230);
		if (!has_booted) {
			frame.setLocation(displayWidth/2 - WINDOW_WIDTH, (displayHeight - WINDOW_HEIGHT) / 2);
			
			if (map_window != null) {
				map_window.addDrawHandler(this, "map_draw");
			}
			
			has_booted = true;
		}
	}
	else {
		println("well, well... Draw happening before this is ready....");
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * */
// Input event handlers
/* * * * * * * * * * * * * * * * * * * * * * * * * */

public void terrain_height_change(GSlider source, GEvent event) {
	// TODO: if "close" to a tick position, snap to it.
	
	terrain_height_readout.setText(source.getValueS() + " meters");
	pixel_buffer_is_outdated = true;
}

public void water_height_change(GSlider source, GEvent event) {
	water_height_readout.setText(source.getValueS() + " meters");
	pixel_buffer_is_outdated = true;
}

public void region_count_x_change(GTextField source, GEvent event) {
	pixel_buffer_is_outdated = true;
}

public void region_count_y_change(GTextField source, GEvent event) {
	pixel_buffer_is_outdated = true;
}

public void load_image_click(GButton source, GEvent event) {
	// create a file chooser
	//final ImageFileLoader loader = new ImageFileLoader();
	//final ImageFilter filter = new ImageFilter();
	
	selectInput("Select a file to process:", "load_image_from_disk");
}

public void export_terrain_click(GButton source, GEvent event) {
	if (map == null || !map_is_ready) {
		return;
	}
	int raw_max_pix_index = 256*256;
	byte[] default_channel_values = {byte(0x00), byte(0x23), byte(0x00), byte(0xFF), byte(0x00), byte(0x00), byte(0x00), byte(0x00), byte(0xFF), byte(0xFF), byte(0x00), byte(0x20), byte(0x20)};
	
	byte[] raw_map = new byte[raw_max_pix_index * default_channel_values.length];
	byte[] raw_color;
	
	float map_value_to_meter = terrain_height.getValueF() / MAP_MAX_VALUE;
	
	float raw_index_to_map_index = map.length / raw_max_pix_index;
	
	int pixel_value;
	int map_index;
	
	for (int pix_index = 0; pix_index < raw_max_pix_index; ++pix_index) {
		// TODO: use a better interpolation algorithm....
		map_index = int(pix_index * raw_index_to_map_index);
		
		pixel_value = map[map_index];
		
		raw_color = find_nearest_color_from_height(pixel_value * map_value_to_meter);
		
		if (raw_color.length <= 0) {
			println("Err... Unable to find a value near ", pixel_value * map_value_to_meter);
			println("pix_index = ", pix_index);
			println("raw_index_to_map_index = ", raw_index_to_map_index);
			println("map_value_to_meter = ", map_value_to_meter);
			println("pixel_value = ", pixel_value);
			println("int(pix_index * raw_index_to_map_index) = ", int(pix_index * raw_index_to_map_index));
			println("map.pixels[0] = ", map[0]);
			raw_color = new byte[2];
			raw_color[0] = default_channel_values[0];
			raw_color[1] = default_channel_values[1];
			exit();
			return;
		}
		
		raw_map[0 + pix_index * default_channel_values.length] = raw_color[0];
		raw_map[1 + pix_index * default_channel_values.length] = raw_color[1];
		raw_map[2 + pix_index * default_channel_values.length] = default_channel_values[2];
		raw_map[3 + pix_index * default_channel_values.length] = default_channel_values[3];
		raw_map[4 + pix_index * default_channel_values.length] = default_channel_values[4];
		raw_map[5 + pix_index * default_channel_values.length] = default_channel_values[5];
		raw_map[6 + pix_index * default_channel_values.length] = default_channel_values[6];
		raw_map[7 + pix_index * default_channel_values.length] = default_channel_values[7];
		raw_map[8 + pix_index * default_channel_values.length] = default_channel_values[8];
		raw_map[9 + pix_index * default_channel_values.length] = default_channel_values[9];
		raw_map[10 + pix_index * default_channel_values.length] = default_channel_values[10];
		raw_map[11 + pix_index * default_channel_values.length] = default_channel_values[11];
		raw_map[12 + pix_index * default_channel_values.length] = default_channel_values[12];
	}
	
	saveBytes("test_terrain.raw", raw_map);
}

synchronized public void map_draw(GWinApplet appc, GWinData data) {
	if (pixel_buffer_is_outdated) {
		update_pixel_buffer();
	}
	
	if (appc != null && pixel_buffer != null) {
		appc.image(pixel_buffer, 0, 0);
	}
}



/* * * * * * * * * * * * * * * * * * * * * * * * * */
// Helper functions
/* * * * * * * * * * * * * * * * * * * * * * * * * */

void update_pixel_buffer() {
	if (!map_is_ready || map_window == null || map_window.papplet == null || map_window.papplet.bkImage == null) {
		return;
	}
	
	map_is_ready = false;
	
	pixel_buffer.width = map_window.papplet.bkImage.width;
	pixel_buffer.height = map_window.papplet.bkImage.height;
	
	pixel_buffer.loadPixels();
	
	float map_step = float(map.length) / pixel_buffer.pixels.length;
	float value_to_meter = terrain_height.getValueF() / MAP_MAX_VALUE;
	
	color pixel_color;
	int pixel_value8;
	float pixel_height;
	int map_index;
	int map_value;
	
	for (int pix_index = 0; pix_index < pixel_buffer.pixels.length; ++pix_index) {
		// TODO: use a more advanced XY expansion/compression algorithm...  This is going to have som BAD aliasing...
		// BUG: if input map isn't the same width as drawing surface there's a duplication pattern issue.  This is because the relative widths are not being compensated for.
		map_index = int(pix_index * map_step);
		
		map_value = map[map_index];
		
		pixel_value8 = (map_value >> MAP_MSB_SHIFT) & 0xFF; // Remove all but the highest byte.
		
		pixel_color = color(pixel_value8);
		
		pixel_height = map_value * value_to_meter;
		
		// Colorize the values.
		if (pixel_height <= water_height.getValueF()) {
			pixel_color = pixel_color | 0xFF; // Set the last channel (blue) to max.
		}
		// TODO: be more creative in colorizing the map.
		
		pixel_buffer.pixels[pix_index] = pixel_color;
	}
	pixel_buffer.updatePixels();
	
	pixel_buffer_is_outdated = false;
	map_is_ready = true;
}

byte[] find_nearest_color_from_height(float height) {
	int index_min = 0; int index_max = 17579; int index_mid;
	JSONObject conversion_a;
	JSONObject conversion_b;
	float conversion_height_a;
	float conversion_height_b;
	
	byte[] return_val = {};
	
	while (index_min <= index_max) {
		index_mid = index_min + (index_max - index_min) / 2;
		conversion_a = terrain_conversions.getJSONObject(index_mid);
		conversion_height_a = conversion_a.getFloat("h");
		conversion_b = terrain_conversions.getJSONObject(index_mid + 1);
		conversion_height_b = conversion_b.getFloat("h");
		
		//println("Checking for height=", height, " at imin=", index_min, " at imid=", index_mid, " at imax=", index_max, ". Got a=", conversion_height_a, " and b=", conversion_height_b);
		
		if (height >= conversion_height_a && height < conversion_height_b) {
			return_val = new byte[2];
			return_val[0] = byte(conversion_a.getInt("r"));
			return_val[1] = byte(conversion_a.getInt("g"));
			return return_val;
		}
		else if (conversion_height_a < height) {
			index_min = index_mid + 1;
		}
		else {
			index_max = index_mid - 1;
		}
	}
	
	return return_val;
}

public void load_image_from_disk(File file) {
	if (file == null) {
		println("load_image_from_disk: input object 'file' was null.");
		return;
	}

	final ImageFilter filter = new ImageFilter();
	String extension = checkExtension(file.getName());
	
	if (filter.is_readable_by_PImage(extension)) {
		println(" Is readable by PImage.");
		// load the image using the given file path
		PImage img = loadImage(file.getPath());
		if (img != null) {
			if (img.width == 256) { // HACK: due to a bug in the drawing code.
				while (!map_is_ready) {
					try {
						Thread.sleep(50);
					}
					catch (InterruptedException e) {
						// Ignore.
					}
				}
				map_is_ready = false;
				
				print(" Processing incoming image...");
				map = new int[img.width * img.height];
				img.loadPixels();
				for (int pixel_index = 0; pixel_index < img.pixels.length; ++pixel_index) {
					map[pixel_index] = int(MAP_MAX_VALUE * brightness(img.pixels[pixel_index]) / 255);
				}
				println(" Done.");
				
				map_is_ready = true;
				pixel_buffer_is_outdated = true;
			}
			else {
				println(" Is wrong size.");
				PDialog.alert("Ugh, I've not yet been coded to handle images that are not 256x256 pixels... so sorry...  Maybe you can help?");
			}
		}
	}
	//else if (is_HDR_image_file(file)) {
	//	println(" Is an HDR image.");
	//	PDialog.alert("Sweet!  An HDR image!  However, I'm not yet able to grok those just yet... Any way you can help?");
	//}
	else {
		println(" Is an unknown image.");
		PDialog.alert("Hmmm... I don't recognize that file type...");
	}
}

void createGUI() {
	G4P.messagesEnabled(false);
	G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
	G4P.setCursor(ARROW);
	if(frame != null)
		frame.setTitle("Sketch Window");
	
	int ROW_HEIGHT = 20;
	int row_index = -1;
	
	++row_index;
	label3 = new GLabel(this, 0, row_index*ROW_HEIGHT, 80, ROW_HEIGHT);
	label3.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
	label3.setText("Region Count");
	label3.setOpaque(false);
	++row_index;
	label4 = new GLabel(this, 10, row_index*ROW_HEIGHT, 20, ROW_HEIGHT);
	label4.setTextAlign(GAlign.RIGHT, GAlign.MIDDLE);
	label4.setText("X:");
	label4.setOpaque(false);
	region_count_x = new GTextField(this, 30, row_index*ROW_HEIGHT, 40, ROW_HEIGHT, G4P.SCROLLBARS_NONE);
	region_count_x.setText("1");
	region_count_x.setDefaultText("1");
	region_count_x.setOpaque(true);
	region_count_x.addEventHandler(this, "region_count_x_change");
	label5 = new GLabel(this, 70, row_index*ROW_HEIGHT, 20, ROW_HEIGHT);
	label5.setTextAlign(GAlign.RIGHT, GAlign.MIDDLE);
	label5.setText("Y:");
	label5.setOpaque(false);
	region_count_y = new GTextField(this, 90, row_index*ROW_HEIGHT, 40, ROW_HEIGHT, G4P.SCROLLBARS_NONE);
	region_count_y.setText("1");
	region_count_y.setDefaultText("1");
	region_count_y.setOpaque(true);
	region_count_y.addEventHandler(this, "region_count_y_change");
	
	++row_index;
	label1 = new GLabel(this, 0, row_index*ROW_HEIGHT, 150, ROW_HEIGHT);
	label1.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
	label1.setText("Water Height");
	label1.setOpaque(false);
	++row_index;
	water_height = new GSlider(this, 10, row_index*ROW_HEIGHT, 255, ROW_HEIGHT, 10.0);
	water_height.setShowValue(true);
	water_height.setShowLimits(true);
	water_height.setLimits(20, 0, 255);
	water_height.setValue(20);
	water_height.setNbrTicks(4);
	water_height.setShowTicks(true);
	//water_height.setStickToTicks(true);
	water_height.setNumberFormat(G4P.INTEGER, 1);
	water_height.setOpaque(false);
	water_height.addEventHandler(this, "water_height_change");
	water_height_readout = new GLabel(this, 265, row_index*ROW_HEIGHT, 80, ROW_HEIGHT);
	water_height_readout.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
	water_height_readout.setOpaque(false);
	water_height_change(water_height, null);
	
	++row_index;
	label2 = new GLabel(this, 0, row_index*ROW_HEIGHT, 150, ROW_HEIGHT);
	label2.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
	label2.setText("Maximum Terrain Height");
	label2.setOpaque(false);
	++row_index;
	terrain_height = new GSlider(this, 10, row_index*ROW_HEIGHT, 255, ROW_HEIGHT, 10.0);
	terrain_height.setShowValue(true);
	terrain_height.setShowLimits(true);
	terrain_height.setLimits(64.0, 0.0, 512.0);
	terrain_height.setValue(64.0);
	terrain_height.setNbrTicks(9);
	terrain_height.setShowTicks(true);
	//terrain_height.setStickToTicks(true);
	terrain_height.setNumberFormat(G4P.DECIMAL, 2);
	terrain_height.setOpaque(false);
	terrain_height.addEventHandler(this, "terrain_height_change");
	terrain_height_readout = new GLabel(this, 265, row_index*ROW_HEIGHT, 100, ROW_HEIGHT);
	terrain_height_readout.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
	terrain_height_readout.setOpaque(false);
	terrain_height_change(terrain_height, null);
	
	++row_index;
	++row_index;
	load_image = new GButton(this, 10, row_index*ROW_HEIGHT, 80, ROW_HEIGHT*2);
	load_image.setText("Load Image");
	load_image.setTextBold();
	load_image.addEventHandler(this, "load_image_click");
	load_image = new GButton(this, WINDOW_WIDTH - 90, row_index*ROW_HEIGHT, 80, ROW_HEIGHT*2);
	load_image.setText("Export Terrain");
	load_image.setTextBold();
	load_image.addEventHandler(this, "export_terrain_click");
}

