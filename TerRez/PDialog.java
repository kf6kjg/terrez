public class PDialog {
	/**
	* Enable or disable the system look-and-feel.
	*/
	public static void setSystemLookAndFeel(final boolean systemLaF) {
		try {
			if (systemLaF) {
				javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
			}
			else {
				javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
			}
		}
		catch (Exception e) {
			//println("Exception trying to set look-and-feel for swing to the ", (systemLaF ? "system" : "cross platform"), " style.");
			e.printStackTrace();
		}
	}
	
	/**
	* A simple modal alert
	*/
	public static void alert(final String title) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					javax.swing.JOptionPane.showMessageDialog(null, title);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

