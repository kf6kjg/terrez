import java.io.File;
import java.util.List;
import processing.core.PApplet;

public class ImageFilter {
	public boolean accept(final File file) {
		if (file != null) {
			// TODO: BUG: File extensions are not the best heuristic - need to use a proper MIME classifier that also checks magic numbers.
			String extension = PApplet.checkExtension(file.getName());
			if (extension != null && extension.length() > 0) {
				return
					this.is_readable_by_PImage(extension) ||
					false
				;
			}
		}
		
		return false;
	}
	
	public boolean is_readable_by_PImage(final String extension) {
		final List<String> accepted_exts = java.util.Arrays.asList(
			"jpeg",
			"jpg",
			"gif",
			"png",
			"tga"
		);
		
		if (extension != null) {
			return accepted_exts.contains(extension);
		}
		
		return false;
	}
}
